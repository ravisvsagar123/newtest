const firebase = require("firebase").default;

try {
  var config = {
    apiKey: "AIzaSyCMELPM37E7BbwwCNLTZXrqm65dOBlNC_k",
    authDomain: "bullforcefirebase.firebaseapp.com",
    projectId: "bullforcefirebase",
    storageBucket: "bullforcefirebase.appspot.com",
    messagingSenderId: "159079487271",
    appId: "1:159079487271:web:453300db27e4949daafb02",
    measurementId: "G-LTKVGC1515"
  };
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }
} catch (e) {
  console.log(e);
}

// var firebaseAuth = firebase.auth;
// var googleAuthProviderId = firebase.auth.GoogleAuthProvider.PROVIDER_ID;
// var phoneAuthProviderID = firebase.auth.PhoneAuthProvider.PROVIDER_ID;
// var facebookAuthProviderId = firebase.auth.FacebookAuthProvider.PROVIDER_ID;
// var twitterAuthProviderId = firebase.auth.TwitterAuthProvider.PROVIDER_ID;
// var githubAuthProviderId = firebase.auth.GithubAuthProvider.PROVIDER_ID;

// var firebaseRef = firebase.database().ref();

const analytics = firebase.analytics();
module.exports = {
  // firebaseAuth,
  // firebaseRef,
  // googleAuthProviderId,
  // phoneAuthProviderID,
  // facebookAuthProviderId,
  // twitterAuthProviderId,
  // githubAuthProviderId
  analytics
};
