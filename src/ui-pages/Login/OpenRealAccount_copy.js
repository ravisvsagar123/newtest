import React, { Component, useState } from 'react'
import {
  Grid, Typography, Button, Box,
  Step, Stepper, StepLabel, Checkbox, IconButton, Snackbar

} from '@material-ui/core'
import accbg from '../../ui-assets/images/Rectangle.svg'
import ArrowBack from "@material-ui/icons/KeyboardArrowLeft"

import CustomTextField from "../../ui-atoms/components/CustomTextField"
import Select from "react-select";
import countries from "../../ui-config/countries.json"
import cfdimg from '../../ui-assets/images/cfd.png'
import empstatus from '../../ui-assets/images/empstatus.png'
import cfdpage3 from '../../ui-assets/images/cfdpage3.png'
import CloseIcon from "@material-ui/icons/Close";
import uploadimg from '../../ui-assets/images/upload.svg'
import selectedfile from '../../ui-assets/images/selectedfile.svg'
import QRCode from 'qrcode.react';
import validator from 'validator'
//import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';

import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';

const styles = {
  mainback: {



    height: '100vh',



    background: 'linear-gradient(119deg, #00cfd2, #006aab 91%, #0060a7)',
  },
  paperContainer: {
    height: '768px',
    width: 'auto',
    backgroundImage: `url(${accbg})`,

    backgroundPosition: 'center',
    margin: '0px 0px',
    backgroundSize: "cover",
  },
  mainbacksub: {
    width: '830px',
    height: '623px',
    background: '#fff',
    borderRadius: '12px',
    justifyContent: 'center',

    margin: '10px 75px 1px 268px'
  },
  mainheadersub: {
    color: "#ffffff",
    fontFamily: "OpenSans-Regular",
    fontSize: "18px",

  },
  demohead: {
    color: "#333333",
    fontFamily: "OpenSans-Regular",
    fontSize: "18px",

  },
  line: {
    border: '#dddddd 1px solid',
  },
  field: {
    marginLeft: "2px",
    marginRight: "2px",
    height: '34px',
    width: '300px',

  },
  mainheader: {
    color: "#ffffff",
    fontFamily: "OpenSans-Bold",
    fontSize: "24px",
  },
  labelContainer: {
    "& $alternativeLabel": {
      marginTop: 0
    }
  },

};


export class PersonalInfo extends Component {

  constructor(props) {
    super(props)

    this.state = {

      isfname: false

    }
  }



  handleInputChange(e) {
    this.props.onNameChange(e.target.value)
  }
  handleLastNameChange(e) {
    this.props.onLastNameChange(e.target.value)
  }
  handleEmailChange(e) {
    this.props.onEmailChange(e.target.value)
  }
  handleMobileChange(e) {
    this.props.onMobChange(e.target.value)
  }
  handleAddChange(e) {
    this.props.onAddChange(e.target.value)
  }
  handlePinCodeChange(e) {
    this.props.onPinCodeCange(e.target.value)
  }
  handleCountryChange(e) {
    this.props.onCountryChange(e.target.value)
  }
  handleStateChange(e) {
    this.props.onStateChange(e.target.value)
  }
  handleCityChange(e) {
    this.props.onCityChange(e.target.value)
  }
  handlePassword1Change(e) {
    this.props.onPassword1Change(e.target.value)
  }

  handlePassword2Change(e) {
    this.props.onPassword2Change(e.target.value)
  }
  render() {

    const select_style = {
      control: base => ({
        ...base,
        border: 0,

        boxShadow: "none",
        fontSize: '14px',
        fontFamily: 'OpenSans-Regular',
        colour: '#999999'
      })
    };
    const countriesData = countries.map(data => {
      return { label: data.country, value: data.countryISO2 };
    });

    const acctype = [
      { value: 'STANDARD', label: 'STANDARD' },
      { value: 'ELITE', label: 'ELITE' },
      { value: 'PREMIUM', label: 'PREMIUM' },
    ]

    const cltype = [
      { value: 'Individual_Account', label: 'Individual Account' },
      { value: 'Common_Account', label: 'Common Account' },
      { value: 'Corporate_Account', label: ' Corporate Account' },
    ]

    const currtype = [
      { value: 'EUR', label: 'EUR' },
      { value: 'USD', label: 'USD' },

    ]

    const { fname, lname, email, mobno, address, pincode, password1, password2, statename, cityname } = this.state;
    return (

      < div >
        <Grid style={styles.line}></Grid>
        <Grid container style={{ height: '95%' }}>
          <Grid item xs={1}></Grid>

          <Grid item xs={4}>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="First Name*"
                value={fname}
                handleChange={e =>
                  this.handleInputChange(e)

                }
              />

            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                value={lname}
                style={styles.field}
                placeholder="Last Name*"
                handleChange={e =>
                  this.handleLastNameChange(e)

                }
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                value={email}
                placeholder="Email*"
                handleChange={e =>
                  this.handleEmailChange(e)

                }
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Mobile No*"
                value={mobno}
                handleChange={e =>
                  this.handleMobileChange(e)

                }
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Address"
                value={address}
                handleChange={e =>
                  this.handleAddChange(e)

                }
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                value={pincode}
                placeholder="Postal code"
                handleChange={e =>
                  this.handlePinCodeChange(e)

                }
              />
            </Grid>
            <Grid style={{ paddingTop: '10px', width: '300px', height: '45px' }}>
              <Select ref={(input) => this.menu = input} styles={select_style} options={countriesData} placeholder="Country"

                onChange={e => {
                  this.setState({ countryname: e.value });
                  this.props.onCountryChange(e.value)
                }}
              />

            </Grid>
          </Grid>

          <Grid item xs={1}></Grid>
          <Grid item xs={4}>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="State"
                value={statename}
                handleChange={e =>
                  this.handleStateChange(e)

                }
              />

            </Grid>
            <Grid style={{ paddingTop: '10px' }} >

              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="City"
                value={cityname}
                handleChange={e =>
                  this.handleCityChange(e)

                }
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select ref={(input) => this.menu = input} styles={select_style} options={currtype} style={{ width: '300px', height: '45px' }} placeholder="Currency choice"

                onChange={e => {
                  this.setState({ currname: e.value });
                  this.props.onCurrTypeChange(e.value)
                }}
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select ref={(input) => this.menu = input} styles={select_style} options={acctype} style={{ width: '300px', height: '45px' }} placeholder="Account Type"
                onChange={e => {
                  this.setState({ acctypename: e.value });
                  this.props.onAccTypeChange(e.value)
                }}


              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select ref={(input) => this.menu = input} styles={select_style} options={cltype} style={{ width: '300px', height: '45px' }} placeholder=" Client Type"

                onChange={e => {
                  this.setState({ clitypename: e.value });
                  this.props.onCliTyChange(e.value)
                }}

              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Password*"
                fullWidth={true}
                type="Password"
                hasEndAdornment={true}
                value={password1}
                handleChange={e =>
                  this.handlePassword1Change(e)

                }
              />
            </Grid>

            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Password Confirmation*"
                fullWidth={true}
                type="Password*"
                hasEndAdornment={true}
                value={password2}
                handleChange={e =>
                  this.handlePassword2Change(e)

                }
              />
            </Grid>

          </Grid>





        </Grid>
      </div >
    )


  }
}
export class AccountType extends Component {
  constructor(props) {
    super(props)

    this.state = {

      isfname: false

    }
  }
  handledobChange(date) {

    this.setState({ dob: date })
    this.props.ondobChange(date)
  }
  handlePlaceofBirthChange(e) {
    this.props.onPlaceofBirthChange(e.target.value)
  }

  handleNationalityChange(e) {
    this.props.onNationalityChange(e.target.value)
  }
  handleCitizenshipChange(e) {
    this.props.onCitizenshipChange(e.target.value)
  }

  handleEducation_levelChange(e) {
    this.props.onEducation_levelChange(e.target.value)
  }

  handleProfessionChange(e) {
    this.props.onProfessionChange(e.target.value)
  }


  render() {
    const select_style = {
      control: base => ({
        ...base,
        border: 0,
        // This line disable the blue border
        boxShadow: "none"
      })
    };
    const empst = [
      { value: 'Employee', label: 'Employee' },
      { value: 'Salaried_employee', label: 'Salaried Employee' },
      { value: 'Self-employed', label: 'Self employed' },
      { value: 'Entrepreneur', label: 'Entrepreneur' },
      { value: 'Employee_Enterpreneus', label: 'Employee & Enterpreneus' },
      { value: 'Unemployed', label: 'Unemployed' },
      { value: 'Student', label: 'Student' },
      { value: 'Retired', label: 'Retired' },
    ]
    const { dob, PlaceofBirth, Nationality, Citizenship, Education_level

    } = this.state
    return (
      <div>
        <Grid style={styles.line}></Grid>
        <Grid container direction="row" style={{ height: '95%', display: 'flex', justifyContent: 'center' }}>
          <Grid>
            <Grid style={{ display: 'flex', paddingTop: '10px' }}>
              <Grid item xs={4} style={{ fontSize: '12px', colour: '#666666', fontFamily: 'OpenSans-Regular', display: 'flex', alignItems: 'center', justifyContent: 'left' }}>Date Of Birth*</Grid>
              <Grid item xs={8}>
                <MuiPickersUtilsProvider

                  style={{ height: '20px' }} utils={DateFnsUtils}>
                  <KeyboardDatePicker

                    value={dob}
                    onChange={date => { this.handledobChange(date) }}
                    format="yyyy/MM/dd"

                    inputVariant="standard"
                    PopoverProps={{
                      style: { backgroundColor: 'red' }
                    }}
                    KeyboardButtonProps={{
                      style: { marginLeft: -24 }
                    }}

                  />
                </MuiPickersUtilsProvider>

              </Grid>
            </Grid>

            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Place of Birth*"
                value={PlaceofBirth}
                handleChange={e =>
                  this.handlePlaceofBirthChange(e)

                }
              />
            </Grid>

            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Nationality*"
                value={Nationality}
                handleChange={e =>
                  this.handleNationalityChange(e)


                }
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Citizenship"
                value={Citizenship}
                handleChange={e =>
                  this.handleCitizenshipChange(e)

                }
              />
            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Education level"
                value={Education_level}
                handleChange={e =>
                  this.handleEducation_levelChange(e)

                }
              />

            </Grid>
            <Grid style={{ paddingTop: '10px' }}>
              <Select ref={(input) => this.menu = input} styles={select_style} options={empst} style={{ width: '300px', height: '45px' }} placeholder="Employment Status"

                onChange={e => {
                  this.setState({ Employment_Status: e.value });
                  this.props.onEmployment_StatusChange(e.value)
                }}


              />
            </Grid>

            <Grid style={{ paddingTop: '10px' }}>
              <CustomTextField
                autoFocus={true}
                style={styles.field}
                placeholder="Profession"
                handleChange={e =>
                  this.handleProfessionChange(e)

                }
              />

            </Grid>
          </Grid>
        </Grid>
        <Grid style={{ paddingTop: '3%' }}></Grid>
      </div>
    )
  }
}
export class Documents extends Component {

  constructor(props) {
    super(props)

    this.state = {


      fname1: '',
      fname2: '',
      fname3: '',
      visible1: false,
      visible2: false,
      visible3: false

    }
  }



  fileSelectHandler1(e) {


    this.setState({ visible1: true, fname1: e.target.files[0].name });

  }
  fileSelectHandler2(e) {

    this.setState({ visible2: true, fname2: e.target.files[0].name });



  }
  fileSelectHandler3(e) {

    this.setState({ visible3: true, fname3: e.target.files[0].name });


  }

  DeleteFile1() {


    this.setState({ visible1: false, fname1: null });

  }

  DeleteFile2() {

    this.setState({ visible2: false, fname2: null });


  }

  DeleteFile3() {

    this.setState({ visible3: false, fname3: null });


  }
  render() {
    const select_style = {
      control: base => ({
        ...base,
        border: 0,
        // This line disable the blue border
        boxShadow: "none",
        fontSize: '14px',
        fontFamily: 'OpenSans-Regular',
        colour: '#999999'
      })
    };

    const profofind = [
      { value: 'Passport', label: 'Passport' },
      { value: 'National_ID', label: 'National ID' },


    ]
    const addverf = [
      { value: 'Credit_Card_Statement', label: 'Credit Card Statement' },
      { value: 'Bank_Statement', label: 'Bank Statement' },
      { value: 'Water_Bill', label: 'Water Bill' },
      { value: 'Phone_Bill', label: 'Phone Bill' },
      { value: 'Electricity_Bill', label: 'Electricity Bill' },
    ]
    const otherdoc = [
      { value: 'DTA', label: 'DTA' },
      { value: 'Credit_Card', label: 'Credit Card' },
      { value: 'Other_Documents', label: 'Other Documents' },

    ]
    const { fname1, fname2, fname3, visible1, visible2, visible3 } = this.state;
    return (
      <div>
        <Grid style={styles.line}></Grid>

        <Grid container direction="row" style={{ height: '95%', paddingTop: '25px', marginLeft: '20px' }}>

          <Grid container style={{ height: '35px' }}>

            <Grid container style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
              <Grid item xs={3}>    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#666666' }}>Proof of Identification*</Typography></Grid>
              <Grid item xs={4}> <Select
                maxMenuHeight="26vh"
                styles={select_style}
                placeholder="Select an option"
                options={profofind}
              /></Grid>
              <Grid container display='flex' item xs={2}>
                <Grid item xs={2}>
                  <img
                    src={uploadimg}
                    alt="filetype"
                    height='20px'
                    width='16px'
                  ></img>
                </Grid>
                <Grid item xs={10}>

                  <input
                    accept="image/*"

                    style={{ display: 'none' }}
                    id="raised-button-file1"
                    multiple
                    type="file"
                    onChange={e => { this.fileSelectHandler1(e) }}
                  />
                  <label htmlFor="raised-button-file1">
                    <Button
                      backgroundColor="transparent"
                      variant="text"
                      component="span"
                      style={{
                        color: "#666666",
                        fontFamily: "OpenSans-Bold",
                        borderRadius: "2px",
                        fontSize: "12px",
                        textTransform: "none",
                        padding: "2px 2px",


                      }}



                    >Choose a file
                    </Button>
                  </label>
                </Grid>
              </Grid>
              <Grid container display='flex' item xs={3}>
                <Grid item xs={2}>
                  {visible1 &&
                    <img
                      src={selectedfile}
                      alt="filetype"
                      height='20px'
                      width='16px'
                    ></img>
                  }
                </Grid>
                <Grid item xs={7}>
                  <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Regular', color: '#666666' }}>{fname1}</Typography>
                </Grid>
                {visible1 &&
                  <Grid item xs={1}>
                    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#00000', cursor: "pointer", }}
                      onClick={() => { this.DeleteFile1() }}
                    >X</Typography>
                  </Grid>
                }
              </Grid>
            </Grid>
          </Grid>
          <Grid container style={{ height: '35px' }}>

            <Grid container style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
              <Grid item xs={3}>    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#666666' }}>Address Verification*</Typography></Grid>
              <Grid item xs={4}>
                <Select
                  maxMenuHeight="26vh"
                  styles={select_style}
                  placeholder="Select an option"
                  options={addverf}
                /></Grid>
              <Grid container display='flex' item xs={2}>
                <Grid item xs={2}>
                  <img
                    src={uploadimg}
                    alt="filetype"
                    height='20px'
                    width='16px'
                  ></img>
                </Grid>
                <Grid item xs={10}>
                  <input
                    accept="image/*"

                    style={{ display: 'none' }}
                    id="raised-button-file2"
                    multiple
                    type="file"
                    onChange={e => { this.fileSelectHandler2(e) }}
                  />
                  <label htmlFor="raised-button-file2">
                    <Button
                      backgroundColor="transparent"
                      variant="text"
                      component="span"
                      style={{
                        color: "#666666",
                        fontFamily: "OpenSans-Bold",
                        borderRadius: "2px",
                        fontSize: "12px",
                        textTransform: "none",
                        padding: "2px 2px",
                      }}


                    >Choose a file
                    </Button>
                  </label>
                </Grid>
              </Grid>
              <Grid container display='flex' item xs={3}>
                <Grid item xs={2}>
                  {visible2 &&
                    <img
                      src={selectedfile}
                      alt="filetype"
                      height='20px'
                      width='16px'
                    ></img>
                  }
                </Grid>
                <Grid item xs={7}>
                  <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Regular', color: '#666666' }}>{fname2}</Typography>
                </Grid>
                {visible2 &&
                  <Grid item xs={1}>
                    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#00000', cursor: "pointer", }}
                      onClick={() => { this.DeleteFile2() }}
                    >X</Typography>
                  </Grid>
                }
              </Grid>
            </Grid>
          </Grid>
          <Grid container style={{ height: '35px' }}>

            <Grid container style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
              <Grid item xs={3}>    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#666666' }}>Other Documents*</Typography></Grid>
              <Grid item xs={4}> <Select
                maxMenuHeight="26vh"
                styles={select_style}
                placeholder="Select an option"
                options={otherdoc}
              /></Grid>
              <Grid container display='flex' item xs={2}>
                <Grid item xs={2}>

                  <img
                    src={uploadimg}
                    alt="filetype"
                    height='20px'
                    width='16px'
                  ></img>

                </Grid>
                <Grid item xs={10}>
                  <input
                    accept="image/*"

                    style={{ display: 'none' }}
                    id="raised-button-file3"
                    multiple
                    type="file"
                    onChange={e => { this.fileSelectHandler3(e) }}
                  />
                  <label htmlFor="raised-button-file3">
                    <Button
                      backgroundColor="transparent"
                      variant="text"
                      component="span"
                      style={{
                        color: "#666666",
                        fontFamily: "OpenSans-Bold",
                        borderRadius: "2px",
                        fontSize: "12px",
                        textTransform: "none",
                        padding: "2px 2px",
                      }}


                    >Choose a file
                    </Button>
                  </label>
                </Grid>
              </Grid>

              <Grid container display='flex' item xs={3}>
                <Grid item xs={2}>
                  {visible3 &&
                    <img
                      src={selectedfile}
                      alt="filetype"
                      height='20px'
                      width='16px'
                    ></img>
                  }
                </Grid>
                <Grid item xs={7}>
                  <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Regular', color: '#666666' }}>{fname3}</Typography>
                </Grid>
                {visible3 &&
                  <Grid item xs={1}>
                    <Typography style={{ fontSize: '14px', fontFamily: 'OpenSans-Bold', color: '#00000', cursor: "pointer", }}
                      onClick={() => { this.DeleteFile3() }}
                    >X</Typography>
                  </Grid>
                }
              </Grid>

              <Grid>

              </Grid>

            </Grid>

          </Grid>

        </Grid>
        <Grid style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>

          <QRCode
            value="https://www.bullforce.co/" style={{ paddingTop: '100px' }} />
        </Grid>
      </div >
    )
  }
}

function IndustryExp(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };

  function Mytab1Click() {
    setMytab1(indstyles.mylinefill)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylineborder)
    setPageNo('1')
  }

  function Mytab2Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylinefill)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylineborder)
    setPageNo('2')
  }
  function Mytab3Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylinefill)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylineborder)
    setPageNo('3')
  }
  function Mytab4Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylinefill)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylineborder)
    setPageNo('4')
  }
  function Mytab5Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylinefill)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylineborder)
    setPageNo('5')
  }
  function Mytab6Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylinefill)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylineborder)
    setPageNo('6')
  }
  function Mytab7Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylinefill)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylineborder)
    setPageNo('7')
  }
  function Mytab8Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylinefill)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylineborder)
    setPageNo('8')
  }
  function Mytab9Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylinefill)
    setMytab10(indstyles.mylineborder)
    setPageNo('9')
  }
  function Mytab10Click() {
    setMytab1(indstyles.mylineborder)
    setMytab2(indstyles.mylineborder)
    setMytab3(indstyles.mylineborder)
    setMytab4(indstyles.mylineborder)
    setMytab5(indstyles.mylineborder)
    setMytab6(indstyles.mylineborder)
    setMytab7(indstyles.mylineborder)
    setMytab8(indstyles.mylineborder)
    setMytab9(indstyles.mylineborder)
    setMytab10(indstyles.mylinefill)
    setPageNo('10')
  }

  const [mytab1, setMytab1] = useState(indstyles.mylinefill);
  const [mytab2, setMytab2] = useState(indstyles.mylineborder);
  const [mytab3, setMytab3] = useState(indstyles.mylineborder);
  const [mytab4, setMytab4] = useState(indstyles.mylineborder);
  const [mytab5, setMytab5] = useState(indstyles.mylineborder);
  const [mytab6, setMytab6] = useState(indstyles.mylineborder);
  const [mytab7, setMytab7] = useState(indstyles.mylineborder);
  const [mytab8, setMytab8] = useState(indstyles.mylineborder);
  const [mytab9, setMytab9] = useState(indstyles.mylineborder);
  const [mytab10, setMytab10] = useState(indstyles.mylineborder);
  const [mypage, setPageNo] = useState('1');
  return (


    <div>
      <Grid style={styles.line}></Grid>
      <Grid container display='flex' justifyContent='center' style={{ paddingTop: '35px' }}>
        <Grid container display='flex' justifyContent='center' style={{ width: '734px', height: '5px' }}>
          <Grid container item xs={1}>
            <Box style={mytab1} onClick={Mytab1Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab2} onClick={Mytab2Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab3} onClick={Mytab3Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab4} onClick={Mytab4Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab5} onClick={Mytab5Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab6} onClick={Mytab6Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab7} onClick={Mytab7Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab8} onClick={Mytab8Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab9} onClick={Mytab9Click}></Box>
          </Grid>
          <Grid container item xs={1}>
            <Box style={mytab10} onClick={Mytab10Click}></Box>
          </Grid>

        </Grid>

      </Grid>
      <Grid>
        {mypage === '1' && <Page1 pageno={setPageNo} p1={setMytab1} p2={setMytab2} />}
        {mypage === '2' && <Page2 pageno={setPageNo} p2={setMytab2} p3={setMytab3} />}
        {mypage === '3' && <Page3 pageno={setPageNo} p3={setMytab3} p4={setMytab4} />}
        {mypage === '4' && <Page4 pageno={setPageNo} p4={setMytab4} p5={setMytab5} />}
        {mypage === '5' && <Page5 pageno={setPageNo} p5={setMytab5} p6={setMytab6} />}
        {mypage === '6' && <Page6 pageno={setPageNo} p6={setMytab6} p7={setMytab7} />}
        {mypage === '7' && <Page7 pageno={setPageNo} p7={setMytab7} p8={setMytab8} />}
        {mypage === '8' && <Page8 pageno={setPageNo} p8={setMytab8} p9={setMytab9} />}
        {mypage === '9' && <Page9 pageno={setPageNo} p9={setMytab9} p10={setMytab10} />}
        {mypage === '10' && <Page10 pageno={setPageNo} p9={setMytab9} p10={setMytab10} />}
      </Grid>
    </div>
  )
}
function Page1(props) {

  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('2');
    props.p1(indstyles.mylineborder);
    props.p2(indstyles.mylinefill);
  }

  return (
    <Grid>
      <Grid style={{ paddingTop: '30px', height: '155px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <img
          src={cfdimg}
          alt="loginb"
          height='155px'
          width='131px'
        ></img>
      </Grid>
      <Grid style={{ paddingTop: '18px', height: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Do you know what is a CFD?*</Typography>
      </Grid>
      <Grid style={{ paddingTop: '18px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#666666",
            fontFamily: "OpenSans-Bold",
            borderRadius: "2px",
            fontSize: "12px",
            textTransform: "none",
            width: '300px',
            padding: "2px 2px",
          }}

          onClick={cp}
        >Yes
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '18px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#666666",
            fontFamily: "OpenSans-Bold",
            borderRadius: "2px",
            fontSize: "12px",
            textTransform: "none",
            padding: "2px 2px",
          }}
          onClick={cp}

        >No
        </Button>

      </Grid>
    </Grid>


  )
}
function Page2(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('3');
    props.p2(indstyles.mylineborder);
    props.p3(indstyles.mylinefill);
  }
  return (
    <Grid>
      <Grid style={{ paddingTop: '20px', height: '133px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <img
          src={empstatus}
          alt="loginb"
          height='133px'
          width='220px'
        ></img>
      </Grid>
      <Grid style={{ paddingTop: '25px', height: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Employment Status*</Typography>
      </Grid>
      <Grid style={{ paddingTop: '2%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "2px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
          }}
          onClick={cp}

        >I hold a professional Qualification in finance/economics
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '2%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "2px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
          }}

          onClick={cp}
        >Received higher education in finance and/ Or financial services
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '2%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "2px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
          }}
          onClick={cp}
        >In the last 3 years, worked in a role relevant to trading derivatives, For over 1 year
        </Button>

      </Grid>
    </Grid>


  )
}


function Page3(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('4');
    props.p3(indstyles.mylineborder);
    props.p4(indstyles.mylinefill);
  }
  return (
    <Grid>
      <Grid style={{ paddingTop: '30px', height: '150px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <img
          src={cfdpage3}
          alt="loginb"
          height='150px'
          width='235px'
        ></img>
      </Grid>
      <Grid style={{ paddingTop: '18px', height: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Trading with a higher leverage in CFD’s
        </Typography>
      </Grid>
      <Grid style={{ paddingTop: '5px', height: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}> Means you may open a larger position volume, Thus increasing the risk of loss*          </Typography>
      </Grid>

      <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '300px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Yes
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '18px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '300px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >No
        </Button>

      </Grid>
    </Grid>


  )
}
function Page4(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('5');
    props.p4(indstyles.mylineborder);
    props.p5(indstyles.mylinefill);
  }
  return (
    <Grid>

      <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
        <Grid>
          <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            Over the past 2 years, to what extent have you traded on your own

          </Typography>
          <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            decision the following financial products
          </Typography>
        </Grid>
      </Grid>
      <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Shares, Bonds, Equities or Exchange Traded Funds</Typography>
      </Grid>
      <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Never
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Rarely: 1-25 Trades A Year
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Sometimes: 25-100 Trades A Year
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Often: Over 100 Trades A Year
        </Button>

      </Grid>
    </Grid>


  )
}
function Page5(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('6');
    props.p5(indstyles.mylineborder);
    props.p6(indstyles.mylinefill);
  }
  return (
    <Grid>

      <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
        <Grid>
          <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            Over the past 2 years, to what extent have you traded on your own

          </Typography>
          <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            decision the following financial products
          </Typography>
        </Grid>
      </Grid>
      <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Derivatives such as Futures, Options, Swaps*</Typography>
      </Grid>
      <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Never
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Rarely: 1-25 Trades A Year
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Sometimes: 25-100 Trades A Year
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Often: Over 100 Trades A Year
        </Button>

      </Grid>
    </Grid>


  )
}
function Page6(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('7');
    props.p6(indstyles.mylineborder);
    props.p7(indstyles.mylinefill);
  }
  return (
    <Grid>

      <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
        <Grid>
          <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            Over the past 2 years, to what extent have you traded on your own

          </Typography>
          <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            decision the following financial products
          </Typography>
        </Grid>
      </Grid>
      <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Forex (FX) Or Rolling Spot FX*</Typography>
      </Grid>
      <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Never
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Rarely: 1-25 Trades A Year
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Sometimes: 25-100 Trades A Year
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Often: Over 100 Trades A Year
        </Button>

      </Grid>
    </Grid>


  )
}
function Page7(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('8');
    props.p7(indstyles.mylineborder);
    props.p8(indstyles.mylinefill);
  }
  return (
    <Grid>

      <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
        <Grid>
          <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            Over the past 2 years, to what extent have you traded on your own

          </Typography>
          <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            decision the following financial products
          </Typography>
        </Grid>
      </Grid>
      <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>CFDs*</Typography>
      </Grid>
      <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Never
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Rarely: 1-25 Trades A Year
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Sometimes: 25-100 Trades A Year
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Often: Over 100 Trades A Year
        </Button>

      </Grid>
    </Grid>


  )
}



function Page8(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('9');
    props.p8(indstyles.mylineborder);
    props.p9(indstyles.mylinefill);
  }
  return (
    <Grid>

      <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
        <Grid>
          <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            Over the past 2 years, to what extent have you traded on your own

          </Typography>
          <Typography style={{ paddingTop: '2px', paddingBottom: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            decision the following financial products
          </Typography>
        </Grid>
      </Grid>
      <Grid style={{ paddingTop: '40px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>What was the average yearly volume of invested amounts of your past</Typography>
      </Grid>
      <Grid style={{ paddingTop: '1px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>transactions in the financial products you selected*</Typography>
      </Grid>


      <Grid style={{ paddingTop: '28px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Never Invested
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Less than $500
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >$500 - $2,500
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >$2,500 - 10,000
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}
          onClick={cp}

        >Over 10,000
        </Button>

      </Grid>

    </Grid>


  )
}
function Page9(props) {
  const indstyles = {
    mylineborder: {
      border: '1px solid #b6d9b6 ',
      borderRadius: '4px',
      height: '5px',
      width: '48px',
      cursor: "pointer",
    },
    mylinefill: {

      borderRadius: '4px',
      height: '8px',
      width: '48px',
      backgroundColor: "#48d74d",
      cursor: "pointer",
    }
  };
  function cp() {

    props.pageno('10');
    props.p9(indstyles.mylineborder);
    props.p10(indstyles.mylinefill);
  }
  return (
    <Grid>

      <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
        <Grid>
          <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            Leagal Information

          </Typography>

        </Grid>
      </Grid>
      <Grid style={{ paddingTop: '45px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Are you A “Politically Exposed person”*</Typography>
      </Grid>
      <Grid style={{ paddingTop: '25px', height: '20px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '16px', color: '#999999' }}>
          Politically exposed persons” means: any person holding prominent public positions
        </Typography>
      </Grid>
      <Grid style={{ paddingTop: '5px', height: '20px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '16px', color: '#999999' }}>

          abroad: heads of state or of government, senior politicians on the national level, senior.
        </Typography>
      </Grid>

      <Grid style={{ paddingTop: '32px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >Yes
        </Button>

      </Grid>
      <Grid style={{ paddingTop: '20px', height: '25px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          backgroundColor="transparent"
          variant="text"
          style={{
            color: "#454871",
            fontFamily: "OpenSans-Bold",
            borderRadius: "16px",
            fontSize: "14px",
            textTransform: "none",
            padding: "2px 2px",
            width: '678px',
            border: '1px solid #dddddd'
          }}

          onClick={cp}
        >No
        </Button>

      </Grid>


    </Grid>


  )
}


function Page10(props) {
  return (
    <Grid>

      <Grid style={{ border: '1px solid #b6d9b6', borderRadius: '4px', marginLeft: '76px', marginTop: '20px', height: '76px', width: '674px', display: 'flex', justifyContent: 'center', alignItems: 'center', justifyItems: 'center' }}>
        <Grid>
          <Typography style={{ paddingTop: '10px', paddingLeft: '40px', fontFamily: 'OpenSans-Bold', fontSize: '18px', color: '#454871', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            Leagal Information

          </Typography>

        </Grid>
      </Grid>
      <Grid style={{ paddingTop: '15px', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '18px', color: '#333333' }}>Which is the country from which deposit will be transferred </Typography>
      </Grid>

      <Grid container style={{ paddingLeft: '18%', display: 'flex', alignItems: 'center', height: '25px' }}>
        <Grid><Checkbox style={{ Color: 'yellow' }} /></Grid>
        <Grid><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#454871' }}>I agree to the</Typography></Grid>
        <Grid style={{ paddingLeft: '1%' }}><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#0083ca' }}>Terms and Conditions*</Typography></Grid>
      </Grid>
      <Grid container style={{ paddingLeft: '18%', display: 'flex', alignItems: 'center', height: '25px' }}>
        <Grid><Checkbox style={{ Color: 'yellow' }} /></Grid>
        <Grid><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#454871' }}>I agree to the</Typography></Grid>
        <Grid style={{ paddingLeft: '1%' }}><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#0083ca' }}>Privacy Policy*</Typography></Grid>
      </Grid>
      <Grid container style={{ paddingLeft: '18%', display: 'flex', alignItems: 'center', height: '25px' }}>
        <Grid><Checkbox style={{ Color: 'yellow' }} /></Grid>
        <Grid><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#454871' }}>I agree to the</Typography></Grid>
        <Grid style={{ paddingLeft: '1%' }}><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#0083ca' }}>Risk Disclosure*</Typography></Grid>
      </Grid>
      <Grid container style={{ paddingLeft: '18%', display: 'flex', alignItems: 'center', height: '25px' }}>
        <Grid><Checkbox style={{ Color: 'yellow' }} /></Grid>
        <Grid><Typography style={{ fontFamily: 'OpenSans-Bold', fontSize: '14px', color: '#454871' }}>I understand and agree with the terms of this contract.</Typography></Grid>


      </Grid>
      <Grid container style={{ paddingLeft: '19.5%', display: 'flex', alignItems: 'center', width: '91%', paddingTop: '3.2%' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '14px', color: '#454871' }}>
          You should not agree or sign the application from if you are unsure as to the effects of
        </Typography>
      </Grid>
      <Grid container style={{ paddingLeft: '19.5%', display: 'flex', alignItems: 'center', width: '91%' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '14px', color: '#454871' }}>
          the terms of busness or the nature of the risk involved, By signing the risk warning you
        </Typography>
      </Grid>
      <Grid container style={{ paddingLeft: '19.5%', display: 'flex', alignItems: 'center', width: '91%' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '14px', color: '#454871' }}>
          are acknowledging to bullforce tha you have recived, read and understood that you
        </Typography>
      </Grid>
      <Grid container style={{ paddingLeft: '19.5%', display: 'flex', alignItems: 'center', width: '91%' }}>
        <Typography style={{ fontFamily: 'OpenSans-Regular', fontSize: '14px', color: '#454871' }}>
          have recived read and understood the contents of the risk warning inits entry
        </Typography>
      </Grid>
    </Grid>


  )
}
export class OpenRealAccount_copy extends Component {
  constructor(props) {
    super(props)

    this.state = {
      activeStep: 0,
      open: false,
      openDialog: false,
      errorMsg: '',
      fname: '',
      lname: '',
      email: '',
      mobno: '',
      address: '',
      pincode: '',
      erropen: false,
      countryname: '',
      statename: '',
      cityname: '',
      currname: '',
      acctypename: '',
      clitypename: '',
      password1: '',
      password2: '',
      dob: '',
      PlaceofBirth: '',
      Nationality: '',
      Citizenship: '',
      Education_level: '',
      Profession: '',
      Employment_Status: ''


    }
  }



  handleNext() {
    var activeStep = this.state.activeStep;
    var isok = '0';

    if (activeStep === 0) {
      var fname = this.state.fname;
      var lname = this.state.lname;
      var email = this.state.email;
      var mobno = this.state.mobno;
      var address = this.state.address;
      var pincode = this.state.pincode;
      var countryname = this.state.countryname;
      var statename = this.state.statename;
      var cityname = this.state.cityname;
      var currname = this.state.currname;
      var acctypename = this.state.acctypename;
      var clitypename = this.state.clitypename;
      var password1 = this.state.password1;
      var password2 = this.state.password2;

      if (!address.length && isok === '0') {

      }
      if (!pincode.length && isok === '0') {

      }
      if (!statename.length && isok === '0') {

      }
      if (!cityname.length && isok === '0') {

      }


      if (!fname.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the Fist Name" })
        this.setState({ erropen: true })
      }

      if (!lname.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the Last Name" })
        this.setState({ erropen: true })
      }
      if (!email.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the email-id" })
        this.setState({ erropen: true })
      }
      if (!validator.isEmail(email) && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the Vailed email-id" })
        this.setState({ erropen: true })

      }
      if (!mobno.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the Mobile Number" })
        this.setState({ erropen: true })
      }
      if (!countryname.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Select the country Name" })
        this.setState({ erropen: true })
      }
      if (!currname.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Select the Currency choice" })
        this.setState({ erropen: true })
      }
      if (!acctypename.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Select the Account Type" })
        this.setState({ erropen: true })
      }
      if (!clitypename.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Select the Client Type" })
        this.setState({ erropen: true })
      }
      if (!password1.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the Password" })
        this.setState({ erropen: true })
      }
      if (!password2.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the Password Confirmation" })
        this.setState({ erropen: true })
      }
      if (password1 !== password2) {
        isok = 1;
        this.setState({ errorMsg: " Password is Not Matching" })
        this.setState({ erropen: true })

      }
    }

    if (activeStep === 1) {
      var dob = this.state.dob;
      var PlaceofBirth = this.state.PlaceofBirth;
      var Nationality = this.state.Nationality;
      var Citizenship = this.state.Citizenship;
      var Education_level = this.state.Education_level;
      var Profession = this.state.Profession;
      var Employment_Status = this.state.Employment_Status;

      if (!dob.length && isok === '0') {

      }
      if (!Citizenship.length && isok === '0') {

      }
      if (!Education_level.length && isok === '0') {

      }
      if (!Profession.length && isok === '0') {

      }
      if (!Employment_Status.length && isok === '0') {

      }

      if (!PlaceofBirth.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the Place of Birth" })
        this.setState({ erropen: true })
      }
      if (!Nationality.length && isok === '0') {
        isok = 1;
        this.setState({ errorMsg: "Enter the Nationality" })
        this.setState({ erropen: true })
      }
    }
    if (isok === '0') {
      this.setState({ activeStep: activeStep + 1 });
    }
    //setSkipped(newSkipped);
  };

  //end of handel Next
  handleNameChange = fname => {
    this.setState({ fname })
  }
  handleLastNameChange = lname => {
    this.setState({ lname })
  }
  handleEmailChange = email => {
    this.setState({ email })
  }
  handleMobileChange = mobno => {
    this.setState({ mobno })
  }
  handleAddChange = address => {
    this.setState({ address })
  }
  handlePinCodeChange = pincode => {
    this.setState({ pincode })
  }
  handleCountryChange = countryname => {
    this.setState({ countryname })
  }
  handleStateChange = statename => {
    this.setState({ statename })
  }
  handleCityChange = cityname => {
    this.setState({ cityname })
  }
  handleCurrTypeChange = currname => {
    this.setState({ currname })
  }
  handleAccTypeChange = acctypename => {
    this.setState({ acctypename })
  }
  handleCliTypeChange = clitypename => {
    this.setState({ clitypename })
  }
  handlePassword1Change = password1 => {
    this.setState({ password1 })
  }
  handlePassword2Change = password2 => {
    this.setState({ password2 })
  }
  handledobChange = dob => {
    this.setState({ dob })
  }

  handlePlaceofBirthChange = PlaceofBirth => {
    this.setState({ PlaceofBirth })
  }

  handleNationalityChange = Nationality => {

    this.setState({ Nationality })
  }
  handleCitizenshipChange = Citizenship => {
    this.setState({ Citizenship })
  }
  handleEducation_levelChange = Education_level => {
    this.setState({ Education_level })
  }
  handleProfessionChange = Profession => {
    this.setState({ Profession })
  }
  handleEmployment_StatusChange = Employment_Status => {
    this.setState({ Employment_Status })
  }

  getStepContent(step) {
    switch (step) {
      case 0:
        return <PersonalInfo
          onNameChange={this.handleNameChange}
          onLastNameChange={this.handleLastNameChange}
          onEmailChange={this.handleEmailChange}
          onMobChange={this.handleMobileChange}
          onAddChange={this.handleAddChange}
          onPinCodeCange={this.handlePinCodeChange}
          onCountryChange={this.handleCountryChange}
          onStateChange={this.handleStateChange}
          onCityChange={this.handleCityChange}
          onCurrTypeChange={this.handleCurrTypeChange}
          onAccTypeChange={this.handleAccTypeChange}
          onCliTypeChange={this.handleCliTypeChange}
          onPassword1Change={this.handlePassword1Change}
          onPassword2Change={this.handlePassword2Change}

        />;
      case 1:
        return <AccountType
          ondobChange={this.handledobChange}
          onPlaceofBirthChange={this.handlePlaceofBirthChange}
          onNationalityChange={this.handleNationalityChange}
          onCitizenshipChange={this.handleCitizenshipChange}
          onEducation_levelChange={this.handleEducation_levelChange}
          onProfessionChange={this.handleProfessionChange}
          onEmployment_StatusChange={this.handleEmployment_StatusChange}
        />;
      case 2:
        return <Documents />;
      case 3:
        return <IndustryExp />;

      default:
        return "Unknown step";
    }


  }
  render() {
    /* const c = {
 
       root: {
         "& .MuiStepIcon-active": { color: "#f4d553" },
         "& .MuiStepIcon-completed": { color: "#48d74d" },
         "& .Mui-disabled .MuiStepIcon-root": { color: "#ffffff" },
         "& .Mui-disabled .MuiStepLabel-iconContainer": { color: "red" },
 
 
       },
       text: {
         color: '#D3D3D3',
       },
     };*/

    const { activeStep, erropen, errorMsg } = this.state
    const steps = ['Personal Info', ' Account Type', 'Documents', 'Industry Exp'];

    return (
      <div>
        <Grid style={styles.paperContainer}>
          <Grid style={{ justifyContent: 'center', display: 'flex' }}>
            <Typography style={styles.mainheader}>Registration. Use form below</Typography>
          </Grid>
          <Grid style={{ justifyContent: 'center', display: 'flex' }}>
            <Typography style={styles.mainheadersub}>Please fill in your details to create a trading account.</Typography>
          </Grid>

          <Grid direction='row' style={styles.mainbacksub}>
            <Grid style={{ height: '11%', marginRight: '2%', marginLeft: '2%', alignSelf: 'start' }}>



              <Stepper activeStep={activeStep} className={{
                root: {
                  "& .MuiStepIcon-active": { color: "#f4d553" },
                  "& .MuiStepIcon-completed": { color: "#48d74d" },
                  "& .Mui-disabled .MuiStepIcon-root": { color: "#ffffff" },
                  "& .Mui-disabled .MuiStepLabel-iconContainer": { color: "red" },


                },
                text: {
                  color: '#D3D3D3',
                }, paddingTop: '15px', borderRadius: '12px'
              }}>
                {steps.map((label, index) => {
                  const stepProps = {};
                  const labelProps = {};


                  return (
                    <Step key={label} {...stepProps}>
                      <StepLabel  {...labelProps}>{label}</StepLabel>
                    </Step>
                  );
                })}
              </Stepper>
            </Grid>
            <Grid style={{ height: '78%', alignSelf: 'center' }}>
              {this.getStepContent(activeStep)}
            </Grid>
            <Grid style={{ display: 'flex', height: '11%', marginRight: '7%', marginLeft: '7%', alignSelf: 'end' }}>
              <Grid item xs={8} style={{ paddingTop: '1%' }}>
                <Button
                  backgroundColor="transparent"
                  variant="text"
                  style={{
                    color: "#2f3253",
                    fontFamily: "OpenSans-Bold",
                    borderRadius: "4px",

                    fontSize: "14px",
                    lineHeight: "19px",
                    textTransform: "none",
                    padding: "12px 16px",
                    marginBottom: "5%"

                  }}


                  onClick={() => { this.handleBack() }}
                  sx={{ mr: 1 }}
                  startIcon={<ArrowBack />}
                >BACK
                </Button></Grid>
              <Grid item xs={2} style={{ paddingTop: '1%' }}>
                <Button
                  backgroundColor="transparent"
                  variant="outlined"
                  fullWidth
                  style={{
                    color: "#2f3253",
                    fontFamily: "OpenSans-Bold",
                    borderRadius: "4px",
                    border: '#454871 1px solid',
                    fontSize: "14px",
                    lineHeight: "19px",
                    textTransform: "none",
                    padding: "12px 16px",
                    marginBottom: "5%",
                    width: '82px'
                  }}>SAVE
                </Button>
              </Grid>
              <Grid item xs={2} style={{ paddingTop: '1%' }}>
                <Button
                  backgroundColor="transparent"
                  variant="contained"
                  fullWidth
                  style={{
                    color: "#2f3253",
                    fontFamily: "OpenSans-Bold",
                    borderRadius: "4px",
                    backgroundColor: "#f4d553",
                    fontSize: "14px",
                    lineHeight: "19px",
                    textTransform: "none",
                    padding: "12px 16px",
                    marginBottom: "5%"

                  }}
                  onClick={() => { this.handleNext() }}
                >NEXT
                </Button>
              </Grid>

            </Grid>

          </Grid>
        </Grid >
        <Snackbar

          anchorOrigin={{
            horizontal: "right",
            vertical: "bottom",
          }}
          open={erropen}
          autoHideDuration={50}
          message={errorMsg}

          action={
            <React.Fragment>

              <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={() => { this.setState({ erropen: false }) }}
              >
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
      </div>
    )
  }
}

export default OpenRealAccount_copy
